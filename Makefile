PROJECT = stm8_tmpl

CC = /usr/bin/sdcc
LD = /usr/bin/sdcc
RM = /usr/bin/rm -f
RMDIR = /usr/bin/rm -fr
MKDIR = /usr/bin/mkdir -p

STM8FLASH = /usr/local/bin/stm8flash 
PROGRAMMER = stlinkv2
MCU = stm8s103f3
F_CPU = 16000000
FMT=ihx

OUTPUT_DIR = ./build
SRC_DIR = ./src

TARGET = $(OUTPUT_DIR)/$(PROJECT).$(FMT)

DEPS = -DF_CPU=$(F_CPU)
LIBSPEC = -lstm8 -mstm8
CFLAGS = $(LIBSPEC) --std-c99 --opt-code-size --disable-warning 126 --disable-warning 110 $(DEPS)

INCDIRS = \
	/usr/share/sdcc/include/ \
	./device \
	./src \
	./libs \

INCLUDE := $(patsubst %,-I%, $(INCDIRS))

SRC = ./src/main.c

# ru_stm8libs
SRC += \

# ru_deviceslib
SRC += \

OBJS = $(SRC:.c=.rel)

# SRC = $(notdir $(wildcard $(SRC_DIR)/*.c))
# OBJS = $(addprefix $(OUTPUT_DIR)/, $(SRC:.c=.rel))

all: $(OUTPUT_DIR) $(TARGET)

$(OUTPUT_DIR):
	$(MKDIR) $(OUTPUT_DIR)

# $(OUTPUT_DIR)/%.rel: %.c
%.rel: %.c
	$(CC) $(CFLAGS) $(INCLUDE) -c $? -o $@

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) --out-fmt-$(FMT) -o $(TARGET) $^

flash: $(TARGET)
	$(STM8FLASH) -c $(PROGRAMMER) -p $(MCU) -s flash -w $(TARGET)

clean:
	$(RM) $(OBJS)
	$(RM) $(OBJS:.rel=.cdb)
	$(RM) $(OBJS:.rel=.lk)
	$(RM) $(OBJS:.rel=.map)
	$(RM) $(OBJS:.rel=.rst)
	$(RM) $(OBJS:.rel=.sym)
	$(RM) $(OBJS:.rel=.asm)
	$(RM) $(OBJS:.rel=.lst)
	$(MKDIR) $(OUTPUT_DIR)

	
