# stm8_tmpl

## How to start:
* clone or download this repo
```sh
git clone git@gitlab.com:stm8ru/stm8_tmpl.git __projec_name__
cd __projec_name__
rm -rf .git
git init 
```
* change project name in README.md
* change project name in Makefile

## libs

* get stm8 libs
```sh
mkdir -p libs && cd libs
git submodule add git@gitlab.com:stm8ru/ru_stm8lib.git 
```
* get devices libs
```sh
mkdir -p libs && cd libs
git submodule add git@gitlab.com:rafal.ulko/ru_deviceslib.git
```
